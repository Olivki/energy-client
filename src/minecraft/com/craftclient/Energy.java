package com.craftclient;

import java.io.File;
import java.util.logging.*;

import com.craftclient.mods.*;
import com.craftclient.panels.*;
import com.craftclient.utils.*;

public class Energy {
	
	/**
	 * 20 ticks ~ 1 second
	 **/
	
	public Logger logger;
	
	public ModHandler mods;
	
	public PanelHandler panels;
	
	public File dirMods;
	
	private static Energy instance;
	
	public Energy() {
		instance = this;
		
		onStart();
	}
	
	public void onStart() {
		setLogger(Logger.getLogger(getClass().getName()));
		getLogger().log(Level.INFO, "Energy has been started!");
		createFolder(new File(Client.getMinecraft().getMinecraftDir(), "/energy/"));
		dirMods = createFolder(new File(Client.getMinecraft().getMinecraftDir(), "/energy/mods/"));
		
		setMods(new ModHandler());
		getMods().initModHandler();
		
		setPanels(new PanelHandler());
		getPanels().initPanelHandler();
	}
	
	public File createFolder(File file) {
		file.mkdirs();
		
		getLogger().log(Level.INFO, "Created the file: " + file.getName() + " at: " + file.getAbsolutePath() + "!");
		
		return file;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public ModHandler getMods() {
		return mods;
	}

	public void setMods(ModHandler mods) {
		this.mods = mods;
	}
	
	public PanelHandler getPanels() {
		return panels;
	}

	public void setPanels(PanelHandler panels) {
		this.panels = panels;
	}

	public File getModsDirectory() {
		return dirMods;
	}
	
	public static Energy getInstance() {
		if(instance == null) {
			instance = new Energy();
		}
		
		return instance;
	}
}