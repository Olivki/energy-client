package com.craftclient.mods;

public class ModCommand {
	
	protected ModBase mod;
	
	protected String command;
	
	protected String usage;
	
	protected String description;
	
	public ModCommand(ModBase mod, String command, String usage, String description) {
		setMod(mod);
		setCommand('.' + command.toLowerCase());
		setUsage(usage.toLowerCase());
		setDescription(description);
	}
	
	public void onCommand(String msg, String... args) {};
	
	public boolean canCommandBeParsed(float value, float min, float max) {
		return (value >= min) && (value <= max);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ModBase getMod() {
		return mod;
	}

	public void setMod(ModBase mod) {
		this.mod = mod;
	}
}