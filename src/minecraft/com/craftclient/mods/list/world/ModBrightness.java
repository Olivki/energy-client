package com.craftclient.mods.list.world;

import net.minecraft.src.*;
import com.craftclient.events.*;
import com.craftclient.events.player.EventUpdate;
import com.craftclient.events.world.*;
import com.craftclient.mods.*;
import com.craftclient.utils.*;

public class ModBrightness extends ModBase {

	public float light = 0.0F;

	public ModBrightness() {
		super("Brightness", ModType.WORLD, true, false);
		setDescription("FUCKING COCK SUCKERS!");
		setArrayName("Brightness (N/A)");
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
		registerEvent(EventGamma.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		light = 0.0F;
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof EventUpdate) {	
			if(light <= 16 - (Client.getLightValue())) {
				light += 0.2F;
			} if(light >= 16 - (Client.getLightValue())) {
				light -= 0.2F;
			}
			setArrayName("Brightness (" + (Client.getFormattedString(light)) + ")");
		} if(event instanceof EventGamma) {
			if(getState()) {		
				((EventGamma)event).setGamma(light);
			} else {
				((EventGamma)event).setGamma(0.0F);
			}
		}
	}
}