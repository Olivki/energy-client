package com.craftclient.mods.list.player;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Block;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Reflector;

import com.craftclient.events.Event;
import com.craftclient.events.player.EventEntityMoved;
import com.craftclient.events.player.EventUpdate;
import com.craftclient.events.render.EventRender3D;
import com.craftclient.mods.ModBase;
import com.craftclient.mods.ModType;
import com.craftclient.utils.Client;
import com.craftclient.utils.Player;

public class ModStep extends ModBase {

	public ModStep() {
		super("Step", ModType.PLAYER, false, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				if(Player.isCollidedHorizontally() && !(Player.isInWater()) && canStep()) {
					Player.setPosition(Player.getX(), Player.getY() + 0.6D, Player.getZ());
				}
			}
		}
	}
	
	public boolean canStep() {
		int x = MathHelper.floor_double(Player.getX() - (Player.getDirection() == 1 ? 1 : 0) + (Player.getDirection() == 3 ? 1 : 0));
		int y = MathHelper.floor_double(Player.getY());
		int z = MathHelper.floor_double(Player.getZ() - (Player.getDirection() == 2 ? 1 : 0) + (Player.getDirection() == 0 ? 1 : 0));
		int id = Client.getWorld().getBlockId(x, y, z);
		return Reflector.hasMethod(50) ? Reflector.callBoolean(98, new Object[] {Block.blocksList[id], Client.getWorld(), 
				Integer.valueOf(x), Integer.valueOf(y), Integer.valueOf(z)}): id == 0 
				|| !(Block.blocksList[id].blockMaterial.isSolid()) && !(Block.blocksList[id].blockMaterial.isLiquid())
				&& id != Block.ladder.blockID && id != Block.vine.blockID && !(Client.getGameSettings().keyBindBack.pressed);
	}
}