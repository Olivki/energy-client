package com.craftclient.mods.list.player;

import com.craftclient.events.Event;
import com.craftclient.mods.ModBase;
import com.craftclient.mods.ModType;

public class ModSprint extends ModBase {

	public ModSprint() {
		super("Sprint", ModType.PLAYER, false, false);
		loadSettings();
	}

	@Override
	public void init() {
		
		
	}

	@Override
	public void onEnabled() {
		
		
	}

	@Override
	public void onDisabled() {
		
		
	}

	@Override
	public void onEvent(Event event) {
		
		
	}
}