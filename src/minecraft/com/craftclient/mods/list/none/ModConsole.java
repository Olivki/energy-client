package com.craftclient.mods.list.none;

import com.craftclient.events.Event;
import com.craftclient.mods.*;
import com.craftclient.screens.GuiConsole;
import com.craftclient.utils.Client;
import com.craftclient.utils.Wrapper;

public class ModConsole extends ModBase {

	public ModConsole() {
		super("Console", ModType.NONE, true, true);
		setKey("LCONTROL");
		loadSettings();
	}

	@Override
	public void init() {
		
	}

	@Override
	public void onEnabled() {
		Client.getMinecraft().displayGuiScreen(new GuiConsole());
		toggle();
	}

	@Override
	public void onDisabled() {
		
	}

	@Override
	public void onEvent(Event event) {
		
	}
}