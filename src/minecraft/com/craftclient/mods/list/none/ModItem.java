package com.craftclient.mods.list.none;

import net.minecraft.src.Enchantment;
import net.minecraft.src.NBTTagString;
import com.craftclient.events.*;
import com.craftclient.mods.*;
import com.craftclient.utils.*;

public class ModItem extends ModBase {

	public ModItem() {
		super("Item", ModType.NONE, true, true, ModController.COMMAND);
		registerCommand(new ModCommand(this, "item", ".item [enchant] [add|list] [id] [level]", 
				"Enchants the current item.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(Client.getController().isInCreativeMode()) {
						if(args[0].equalsIgnoreCase("item")) {
							if(args[1].equalsIgnoreCase("enchant")) {
								if(args[2].equalsIgnoreCase("add")) {
									if(Client.getPlayer().inventory.getCurrentItem() != null) {
										Client.getPlayer().inventory.getCurrentItem().addEnchantment(
												Enchantment.enchantmentsList[Integer.parseInt(args[3])], Integer.parseInt(args[4]));
										Player.addMessage("Added the enchantment: " 
												+ Enchantment.enchantmentsList[Integer.parseInt(args[3])].getTranslatedName(Integer.parseInt(
														args[4])) + " to your current item!");
									} else {
										Player.addMessage("Please have a item selected!");
									}
								} if(args[0].equalsIgnoreCase("item")) {
									if(args[1].equalsIgnoreCase("enchant")) {
										if(args[2].equalsIgnoreCase("list")) {
											for(int var0 = 0; var0 < Enchantment.enchantmentsList.length; var0++) {
												Enchantment enchantment = (Enchantment)Enchantment.enchantmentsList[var0];
												if(enchantment != null) {
													Player.addMessage("[" + Colours.DARK_GREEN + var0 + Colours.RESET + "] " 
															+ enchantment.getTranslatedName(var0));
												}
											}
										}
									}
								}
							}
						}
					} else {
						Player.addMessage("You can't use this command unless your in creative.");
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
		registerCommand(new ModCommand(this, "item", ".item [name] [set] [name]", 
				"Sets your current items name.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(Client.getController().isInCreativeMode()) {
						if(args[0].equalsIgnoreCase("item")) {
							if(args[1].equalsIgnoreCase("name")) {
								if(args[2].equalsIgnoreCase("set")) {
									if(Client.getPlayer().inventory.getCurrentItem() != null) {
										Client.getPlayer().inventory.getCurrentItem().setItemName(Colours.RESET 
												+ msg.substring(14).replaceAll("&", "�"));
										Player.addMessage("Your current items name has been set to: " + msg.substring(14).replaceAll("&", "�"));
									} else {
										Player.addMessage("Please have a item selected!");
									}
								}
							}
						}
					} else {
						Player.addMessage("You can't use this command unless your in creative.");
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
	}

	@Override
	public void init() {

	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {

	}
}