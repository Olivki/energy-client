package com.craftclient.mods.list.none;

import com.craftclient.events.*;
import com.craftclient.mods.*;
import com.craftclient.utils.*;

public class ModLogin extends ModBase {

	public ModLogin() {
		super("Login", ModType.NONE, true, true, ModController.COMMAND);
		registerCommand(new ModCommand(this, "login", ".login [username] [password]", 
				"Login you into your account.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(args[0].equalsIgnoreCase("login")) {
						Client.loginToAccount(args[1], args[2]);
						Player.addMessage("Logged into the account!");
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
	}

	@Override
	public void init() {

	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {

	}
}