package com.craftclient.mods.list.none;

import com.craftclient.events.*;
import com.craftclient.mods.*;
import com.craftclient.utils.*;

public class ModSpam extends ModBase {

	public ModSpam() {
		super("Spam", ModType.NONE, true, true, ModController.COMMAND);
		registerCommand(new ModCommand(this, "spam", ".spam [value] [text]", 
				"Used for getting help on a certain mod.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(msg.length() > getName().length()) {
						if(args[0].equalsIgnoreCase("spam")) {
							for(int value = 0; value < Integer.parseInt(args[1]); value++) {
								Player.sendMessage(msg.substring((args[0].length() + args[1].length() + 2)));
							}
						}
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
	}

	@Override
	public void init() {

	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {

	}
}