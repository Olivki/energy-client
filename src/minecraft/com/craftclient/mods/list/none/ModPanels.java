package com.craftclient.mods.list.none;

import com.craftclient.events.Event;
import com.craftclient.mods.*;
import com.craftclient.utils.Client;
import com.craftclient.utils.Wrapper;

public class ModPanels extends ModBase {

	public ModPanels() {
		super("Panels", ModType.NONE, true, true);
		setKey("RSHIFT");
		loadSettings();
	}

	@Override
	public void init() {
		
	}

	@Override
	public void onEnabled() {
		Client.getMinecraft().displayGuiScreen(Wrapper.getEnergy().getPanels());
		toggle();
	}

	@Override
	public void onDisabled() {
		
	}

	@Override
	public void onEvent(Event event) {
		
	}
}