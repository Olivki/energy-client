package com.craftclient.mods.list.render;

import org.lwjgl.opengl.GL11;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.EntityPlayer;

import com.craftclient.events.Event;
import com.craftclient.events.render.EventRenderNametag;
import com.craftclient.events.render.EventRenderPlayer;
import com.craftclient.mods.*;
import com.craftclient.mods.list.none.ModFriend;
import com.craftclient.utils.Client;
import com.craftclient.utils.Colours;
import com.craftclient.utils.Player;
import com.craftclient.utils.Wrapper;

public class ModESP extends ModBase {

	public ModESP() {
		super("ESP", ModType.RENDER, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventRenderNametag.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventRenderNametag) {
				EntityPlayer player = ((EventRenderNametag)event).getPlayer();
				double x = ((EventRenderNametag)event).getX();
				double y = ((EventRenderNametag)event).getY();
				double z = ((EventRenderNametag)event).getZ();

				if(Player.getTarget() instanceof EntityPlayer && Player.getTarget() == player) {
					registerSetting(0, 0xFFEE6363, false, false);
				} else if(Player.getTarget() != player) {
					registerSetting(0, 0xFFFFFFFF, false, false);
				} if(((ModFriend)Wrapper.getEnergy().getMods().getMod("Friend")).isFriend(player.username)) {
					registerSetting(0, 0xFF4F94CD, false, false);
				} if(player.hurtTime > 0) {
					registerSetting(0, 0xFFFFA500, false, false);
				}

				Client.enableDefaults();
				//Client.glColor4Hex(((Integer)getSetting(0)));
				GL11.glColor4f(Colours.getRGBA(((Integer)getSetting(0)))[0], 
						Colours.getRGBA(((Integer)getSetting(0)))[1], Colours.getRGBA(((Integer)getSetting(0)))[2],
						(float) ((Player.getDistanceToEntity(player) / 255) * 40));
				GL11.glTranslated(x + 0.5D, y, z + 0.5D);
				GL11.glRotatef(-player.rotationYaw, 0, 1, 0);
				GL11.glTranslated(-x - 0.5D, -y, -z - 0.5D);
				GL11.glLineWidth(1.5F);
				Client.renderOutlinedBox(AxisAlignedBB.getBoundingBox(x, y, z, x + 1.0D, y + 2.0D, z + 1.0D));
				Client.renderCrossedBox(AxisAlignedBB.getBoundingBox(x, y, z, x + 1.0D, y + 2.0D, z + 1.0D));
				Client.disableDefaults();
			}
		}
	}
}