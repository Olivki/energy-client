package com.craftclient.mods.list.render;

import org.lwjgl.opengl.GL11;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.EntityPlayer;

import com.craftclient.events.Event;
import com.craftclient.events.render.EventRender3D;
import com.craftclient.events.render.EventRenderNametag;
import com.craftclient.events.render.EventRenderPlayer;
import com.craftclient.mods.*;
import com.craftclient.mods.list.none.ModFriend;
import com.craftclient.utils.Client;
import com.craftclient.utils.Colours;
import com.craftclient.utils.Player;
import com.craftclient.utils.Wrapper;

public class ModTracer extends ModBase {

	public ModTracer() {
		super("Tracer", ModType.RENDER, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventRender3D.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventRender3D) {
				for(Object players : Client.getWorld().playerEntities) {
					EntityPlayer player = (EntityPlayer)players;

					if(player != Client.getPlayer()) {
						if(Player.getTarget() instanceof EntityPlayer && Player.getTarget() == player) {
							registerSetting(0, 0xFFEE6363, false, false);
						} else if(Player.getTarget() != player) {
							registerSetting(0, 0xFFFFFFFF, false, false);
						} if(((ModFriend)Wrapper.getEnergy().getMods().getMod("Friend")).isFriend(player.username)) {
							registerSetting(0, 0xFF4F94CD, false, false);
						} if(player.hurtTime > 0) {
							registerSetting(0, 0xFFFFA500, false, false);
						}

						Client.drawTracerTo(player.posX, player.posY + player.getEyeHeight() - 0.5F, player.posZ, 
								(Player.getDistanceToEntity(player) / 255) * 40, Colours.getRGBA(((Integer)getSetting(0)))[0], 
								Colours.getRGBA(((Integer)getSetting(0)))[1], Colours.getRGBA(((Integer)getSetting(0)))[2], 1.5F, player);
					}
				}
			}
		}
	}
}