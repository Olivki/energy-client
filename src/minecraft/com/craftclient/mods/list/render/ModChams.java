package com.craftclient.mods.list.render;

import com.craftclient.events.Event;
import com.craftclient.mods.*;

public class ModChams extends ModBase {

	public ModChams() {
		super("Chams", ModType.RENDER, true, false);
		setArrayName("Chams (Player)");
		loadSettings();
	}

	@Override
	public void init() {
		
	}

	@Override
	public void onEnabled() {
		
	}

	@Override
	public void onDisabled() {
		
	}

	@Override
	public void onEvent(Event event) {
		
	}
}