package com.craftclient.mods.list.gui;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.minecraft.src.*;

import com.craftclient.events.*;
import com.craftclient.events.misc.EventKeyPressed;
import com.craftclient.events.render.*;
import com.craftclient.mods.*;
import com.craftclient.panels.PanelBase;
import com.craftclient.panels.PanelHandler;
import com.craftclient.utils.*;

public class ModGui extends ModBase {

	public ModGui() {
		super("Gui", ModType.GUI, true, true);
		setState(true);
		registerSetting(0, "Minecraft 1.4.6", false);
		registerCommand(new ModCommand(this, "gui", ".gui [title] [new title]", 
				"Used for toggling the GUI or setting the title of it.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(msg.length() > getName().replace(" ", "").length()) {
						if(args[0].equalsIgnoreCase("gui")) {
							if(args[1].equalsIgnoreCase("title")) {
								registerSetting(0, msg.substring("gui title ".length()), true);
								Player.addSettingsMessage("Gui Title ", msg.substring("gui title ".length()));
							}
						}
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
		registerSetting(1, Boolean.valueOf(true), false);
		registerSetting(2, 3, false);
		registerCommand(new ModCommand(this, "gui", ".gui [style] [id]", 
				"Used for setting the guis style.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(msg.length() > getName().replace(" ", "").length()) {
						if(args[0].equalsIgnoreCase("gui")) {
							if(args[1].equalsIgnoreCase("style")) {
								if(Integer.parseInt(args[2]) < 5) {
									registerSetting(2, Integer.parseInt(args[2]), true);
									Player.addSettingsMessage("Gui Style ", "" + getSetting(2));
								} else {
									Player.addMessage("Unknown ID!");
								}
							}
						}
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
		registerSetting(3, 0, false, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventRender2D.class);
		registerEvent(EventKeyPressed.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventRender2D) {
				for(PanelBase panel : Wrapper.getEnergy().getPanels().getLoadedPanels()) {
					if(panel.isPinned() && panel.isOpen()) {
						if(!(Client.getMinecraft().currentScreen instanceof PanelHandler)) {
							panel.draw(-200, -200);
						}
					}
				} switch(((Integer)getSetting(2))) {
				case 0:
					Client.drawStringWithShadow(((String)getSetting(0)), 2, 2, 0xFFFFFFFF);

					if(((Boolean)getSetting(1)).booleanValue()) {
						for(int var0 = 0; var0 < Wrapper.getEnergy().getMods().getActiveMods().size(); var0++) {
							ModBase mod = (ModBase)Wrapper.getEnergy().getMods().getActiveMods().get(var0);

							if(!(mod.isHidden())) {
								Client.drawStringWithShadow(mod.getType().getColour() + mod.getName(), 
										Client.getScaledWidth() - Client.getStringWidth(mod.getType().getColour() + mod.getName()) - 2, 
										2 + var0 * 10, 0xFFFFFFFF);
							}
						}
					}
					break;

				case 1:
					Client.drawStringWithShadow(((String)getSetting(0)) + " (" + (int)Player.getX() + "," + (int)Player.getY()
							+ "," + (int)Player.getZ() + ") " + "(" + Client.getCurrentBiomeName() + ")", 2, 2, 0xFFFFFFFF);

					Client.drawStringWithShadow(Colours.GREY + "Direction: ", 2, 12, 0xFFFFFFFF);
					Client.drawStringWithShadow(Colours.GREY + "Time: ", Client.getStringWidth(Colours.GREY + "Direction:") + 20, 12, 
							0xFFFFFFFF);
					Client.drawStringWithShadow(Colours.GREY + "NoCheat: " + Wrapper.getEnergy().mods.getMod("NoCheat").getState(), 2, 22,
							0xFFFFFFFF);

					Client.drawItem(new ItemStack(Item.compass, 1, 0), Client.getStringWidth(Colours.GREY + "Direction:") + 2, 8);
					Client.drawItem(new ItemStack(Item.pocketSundial, 1, 0), 88, 8);

					if(((Boolean)getSetting(1)).booleanValue()) {
						for(int var0 = 0; var0 < Wrapper.getEnergy().getMods().getActiveMods().size(); var0++) {
							ModBase mod = (ModBase)Wrapper.getEnergy().getMods().getActiveMods().get(var0);

							if(!(mod.isHidden())) {
								Client.drawStringWithShadow(Colours.GREY + mod.getName(), 2, 32 + var0 * 10, 0xFFFFFFFF);
							}
						}
					}
					break;

				case 2:
					Client.getIngameGui().drawRect(0, 0, Client.getScaledWidth(), 12, 0x90161616);
					Client.getIngameGui().drawCenteredStringS(Client.getFontRenderer(), ((String)getSetting(0)), 
							Client.getScaledWidth() / 2, 2, 0xFF1E90FF);

					if(((Boolean)getSetting(1)).booleanValue()) {
						for(int var0 = 0; var0 < Wrapper.getEnergy().getMods().getActiveMods().size(); var0++) {
							ModBase mod = (ModBase)Wrapper.getEnergy().getMods().getActiveMods().get(var0);

							if(!(mod.isHidden())) {
								Client.getIngameGui().drawRect(1, 13 + var0 * 13, 99, 25 + var0 * 13, 0x90161616);
								Client.getIngameGui().drawRect(97, 13 + var0 * 13, 99, 25 + var0 * 13, 0xFF00CC00);
								Client.drawString(Colours.WHITE + StringUtils.stripControlCodes(mod.getArrayName()), 4, 15 + var0 * 13,
										0xFFFFFFFF);
							}
						}
					}
					break;

				case 3:
					Client.drawStringWithShadow(((String)getSetting(0)), 2, 2, 0xFFFFFFFF);

					Client.getIngameGui().drawGradientRect(Client.getScaledWidth() - 89, 0, Client.getScaledWidth(), 
							Wrapper.getEnergy().getMods().getActiveMods().size() * 12 + 2, 1610612736, 1610612736);

					if(((Boolean)getSetting(1)).booleanValue() && !(Wrapper.getEnergy().getMods().getActiveMods().isEmpty())) {
						for(int var0 = 0; var0 < Wrapper.getEnergy().getMods().getActiveMods().size(); var0++) {
							ModBase mod = (ModBase)Wrapper.getEnergy().getMods().getActiveMods().get(var0);

							if(!(mod.isHidden())) {
								int posY = var0 * 12;
								Client.getIngameGui().drawGradientRect(Client.getScaledWidth() - 72, 2 + var0 * 12, Client.getScaledWidth(), 
										posY + 10, 1610612736, 1613441835);
								Client.getIngameGui().drawGradientRect(Client.getScaledWidth() - 72, 2 + var0 * 12 + 8, Client.getScaledWidth(), 
										posY + 12, 1613441835, 1610612736);
								Client.drawStringWithShadow(mod.getName(), Client.getScaledWidth() 
										- Client.getStringWidth(mod.getName()) - 2, 3 + posY, 0xFFFFFFFF);
								Client.drawItem(new ItemStack(Block.ladder), Client.getScaledWidth() - 90, posY - 2);
							}
						}
					}
					break;

				case 4:	
					drawBorderedRect(1, 1, 101, 15, 553648127, -2147483648);
					//drawBorderedRect(1, 15, 101, ModType.values().length * 12 - 10, 553648127, -2147483648);

					for(int var0 = 0; var0 < ModType.values().length; var0++) {
						ModType type = ModType.values()[var0];

						if(type != ModType.NONE) {
							drawBorderedRect(1, 15 + var0 * 12, 101, 28 + var0 * 12, 553648127, -2147483648);
							Client.drawStringWithShadow((((Integer)getSetting(3)) == var0 ? Colours.BRIGHT_GREEN + type.getName() : type.getName()), 
									4, 17.5F + var0 * 12, 0xFFFFFFFF);
							Client.drawStringWithShadow((((Integer)getSetting(3)) == var0 ? Colours.BRIGHT_GREEN + "<<" : Colours.RESET + ">>"), 
									89, 17.5F + var0 * 12, 0xFFFFFFFF);
						}
					}

					Client.drawStringWithShadow(Colours.BRIGHT_GREEN + "Nodus " + Colours.RESET + "v10", 4, 4, 0xFFFFFFFF);
					break;

				default:
					Client.drawStringWithShadow(((String)getSetting(0)), 2, 2, 0xFFFFFFFF);

					if(((Boolean)getSetting(1)).booleanValue()) {
						for(int var0 = 0; var0 < Wrapper.getEnergy().mods.getActiveMods().size(); var0++) {
							ModBase mod = (ModBase)Wrapper.getEnergy().mods.getActiveMods().get(var0);

							if(!(mod.isHidden())) {
								Client.drawStringWithShadow(mod.getArrayName(), Client.getScaledWidth() - Client.getStringWidth(mod.getArrayName()) - 2, 
										2 + var0 * 10, 0xFFFFFFFF);
							}
						}
					}
					break;
				}
			} if(event instanceof EventKeyPressed) {
				if(((EventKeyPressed)event).getKeyID() == (Keyboard.KEY_DOWN)) {
					if(((Integer)getSetting(3)) < (ModType.values().length - 2)) {
						registerSetting(3, ((Integer)getSetting(3)) + 1, true, false);
					} else {
						registerSetting(3, 0, true, false);
					}
				} if(((EventKeyPressed)event).getKeyID() == (Keyboard.KEY_UP)) {
					if(((Integer)getSetting(3)) > 0) {
						registerSetting(3, ((Integer)getSetting(3)) - 1, true, false);
					} else {
						registerSetting(3, (ModType.values().length - 2), true, false);
					}
				}
			}
		}
	}

	public void drawSideGradientRect(int x, int y, int width, int height, int hex1, int hex2) {
		GL11.glPushMatrix();
		float var6 = (float)(hex1 >> 24 & 255) / 255.0F;
		float var7 = (float)(hex1 >> 16 & 255) / 255.0F;
		float var8 = (float)(hex1 >> 8 & 255) / 255.0F;
		float var9 = (float)(hex1 & 255) / 255.0F;
		float var10 = (float)(hex2 >> 24 & 255) / 255.0F;
		float var11 = (float)(hex2 >> 16 & 255) / 255.0F;
		float var12 = (float)(hex2 >> 8 & 255) / 255.0F;
		float var13 = (float)(hex2 & 255) / 255.0F;
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		Tessellator var14 = Tessellator.instance;
		var14.startDrawingQuads();
		var14.setColorRGBA_F(var7, var8, var9, var6);
		var14.addVertex((double)width, (double)y, 0.0D);
		var14.setColorRGBA_F(var11, var12, var13, var10);
		var14.addVertex((double)x, (double)y, 0.0D);
		var14.addVertex((double)x, (double)height, 0.0D);
		var14.setColorRGBA_F(var7, var8, var9, var6);
		var14.addVertex((double)width, (double)height, 0.0D);
		var14.draw();
		GL11.glShadeModel(GL11.GL_FLAT);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glPopMatrix();
	}

	public void drawBorderedRect(float x, float y, float width, float height, int hex1, int hex2) {
		Client.getIngameGui().drawRect(x + 1, y + 1, width - 1, height - 1, hex2);
		Client.getIngameGui().drawVerticalLine(x, y, height - 1, hex1);
		Client.getIngameGui().drawVerticalLine(width - 1, y, height - 1, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, y, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, height - 1, hex1);
	}

	class Animation {

		protected int posY;
		protected int anim;
		protected int max;

		Animation(int max) {
			setMax(max);
		}
		
		Animation() {
			
		}
		
		public void updateAnimation() {
			if(getAnim() < getMax()) {
				setAnim(getAnim() + 1);
				Player.addMessage("" + getAnim());
			}
		}

		public int getPosY() {
			return posY;
		}

		public void setPosY(int posY) {
			this.posY = posY;
		}

		public int getAnim() {
			return anim;
		}

		public void setAnim(int anim) {
			this.anim = anim;
		}

		public int getMax() {
			return max;
		}

		public void setMax(int max) {
			this.max = max;
		}
	}
}