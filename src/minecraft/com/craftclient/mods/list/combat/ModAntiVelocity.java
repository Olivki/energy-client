package com.craftclient.mods.list.combat;

import com.craftclient.events.*;
import com.craftclient.events.player.*;
import com.craftclient.mods.*;
import com.craftclient.utils.*;

public class ModAntiVelocity extends ModBase {

	public ModAntiVelocity() {
		super("Anti Velocity", ModType.COMBAT, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventVelocity.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventVelocity) {
				((EventVelocity)event).setCancelled(true);
			}
		}
	}
}