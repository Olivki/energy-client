package com.craftclient.mods.list.combat;

import net.minecraft.src.*;

import com.craftclient.events.*;
import com.craftclient.events.controller.*;
import com.craftclient.mods.*;
import com.craftclient.mods.list.none.ModFriend;
import com.craftclient.utils.*;

public class ModCriticals extends ModBase {

	public ModCriticals() {
		super("Criticals", ModType.COMBAT, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventAttackEntity.class);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventAttackEntity) {
				if(((EventAttackEntity)event).getTarget() instanceof EntityLiving) {
					if(Player.isOnGround() && !(Player.isJumping()) && !(((ModFriend)Wrapper.getEnergy().getMods().getMod("Friend"))
							.isFriend(((EventAttackEntity)event).getTarget().getEntityName()))) {
						Wrapper.getPlayer().setPosition(Player.getX(), Player.getY() + 0.8D, Player.getZ());
					}
				}
			}
		}
	}
}