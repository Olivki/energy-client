package com.craftclient.mods.list.combat;

import com.craftclient.events.*;
import com.craftclient.events.player.*;
import com.craftclient.mods.*;
import com.craftclient.utils.Client;
import com.craftclient.utils.Player;

public class ModAimbot extends ModBase {

	public ModAimbot() {
		super("Aimbot", ModType.COMBAT, true, false);
		registerSetting(0, true, false);
		registerCommand(new ModCommand(this, "aimbot", ".aimbot [setting] [silent]", 
				"Changes the setting if you want a silent aimbot or not.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(msg.length() > getName().replace(" ", "").length()) {
						if(args[0].equalsIgnoreCase("aimbot")) {
							if(args[1].equalsIgnoreCase("setting")) {
								if(args[2].equalsIgnoreCase("silent")) {
									registerSetting(0, !(((Boolean)getSetting(0))), true);
									Player.addSettingsMessage("Aimbot Silent ", "" + ((Boolean)getSetting(0)));
								}
							}
						}
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
		setArrayName("Aimbot (Player)");
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
		registerEvent(EventPostUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		Player.setTarget(null);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				onUpdate();
			} if(event instanceof EventPostUpdate) {
				onPostUpdate();
			}
		}
	}

	public void onUpdate() {
		if(Player.getTarget() == null) {
			Player.setTarget(Player.getNearestPlayer());
		} else if(!(Client.getWorld().loadedEntityList.contains(Player.getTarget()))) {
			Player.setTarget(null);
		} else if(Player.getDistanceToEntity(Player.getTarget()) >= 3.5F) {
			Player.setTarget(null);
		} if(Player.getTarget() != null) {
			if(Player.getTargetChecks(Player.getTarget())) {
				if(((Boolean)getSetting(0))) {
					registerSetting(1, Player.getRotationPitch(), false, false);
					registerSetting(2, Player.getRotationYaw(), false, false);
					registerSetting(3, Player.getRotationYawHead(), false, false);
					Player.setRotationPitch(Player.getAttackRotationPitch(Player.getTarget()));
					Player.setRotationYaw(Player.getAttackRotationYaw(Player.getTarget()));
				} else {
					Player.setRotationPitch(Player.getAttackRotationPitch(Player.getTarget()));
					Player.setRotationYaw(Player.getAttackRotationYaw(Player.getTarget()));	
				}
			} else {
				Player.setTarget(null);
			}
		}
	}

	public void onPostUpdate() {
		if(Player.getTarget() != null) {
			if(Player.getTargetChecks(Player.getTarget())) {
				if(((Boolean)getSetting(0))) {
					Player.setRotationPitch(((Float)getSetting(1)));
					Player.setRotationYaw(((Float)getSetting(2)));
					Player.setRotationYawHead(((Float)getSetting(3)));
				}
			} else {
				Player.setTarget(null);
			}
		}
	}
}