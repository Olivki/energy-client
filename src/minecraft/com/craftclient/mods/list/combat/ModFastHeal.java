package com.craftclient.mods.list.combat;

import net.minecraft.src.Packet10Flying;

import com.craftclient.events.*;
import com.craftclient.events.player.*;
import com.craftclient.mods.*;
import com.craftclient.utils.*;

public class ModFastHeal extends ModBase {

	public ModFastHeal() {
		super("Fast Heal", ModType.COMBAT, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				if(!(Player.isInWater()) && Player.isOnGround()) {
					if(Client.getPlayer().getHealth() <= 20 && Client.getPlayer().getFoodStats().getFoodLevel() >= 16) {
						for(int var0 = 0; var0 < 5000; var0++) {
							Client.sendPacket(new Packet10Flying(false));
						}
					}			
				}
			}
		}
	}
}