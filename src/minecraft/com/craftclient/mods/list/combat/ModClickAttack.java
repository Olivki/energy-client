package com.craftclient.mods.list.combat;

import com.craftclient.events.Event;
import com.craftclient.events.controller.EventAttackEntity;
import com.craftclient.events.player.*;
import com.craftclient.mods.*;
import com.craftclient.utils.Client;
import com.craftclient.utils.Player;
import com.craftclient.utils.Wrapper;

public class ModClickAttack extends ModBase {

	public ModClickAttack() {
		super("Click Attack", ModType.COMBAT, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
		registerEvent(EventPostUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		Player.setTarget(null);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				if(!(Wrapper.getEnergy().getMods().getMod("Aimbot").getState())) {
					((ModAimbot)Wrapper.getEnergy().getMods().getMod("Aimbot")).onUpdate();
				} if(Player.getTarget() != null) {
					if(Player.isSwinging()) {
						if(Wrapper.getEnergy().getMods().getMod("AutoTool").getState()) {
							Wrapper.getEnergy().getMods().getMod("AutoTool").onEvent(new EventAttackEntity(Player.getTarget()));
						}

						Player.attackEntity(Player.getTarget());
					}
				}
			} if(event instanceof EventPostUpdate) {
				if(!(Wrapper.getEnergy().getMods().getMod("Aimbot").getState())) {
					((ModAimbot)Wrapper.getEnergy().getMods().getMod("Aimbot")).onPostUpdate();
				}
			}
		}
	}
}