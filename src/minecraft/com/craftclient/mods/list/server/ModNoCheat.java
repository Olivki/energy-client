package com.craftclient.mods.list.server;

import com.craftclient.events.*;
import com.craftclient.events.player.*;
import com.craftclient.mods.*;
import com.craftclient.utils.Wrapper;

public class ModNoCheat extends ModBase {

	public ModNoCheat() {
		super("NoCheat", ModType.SERVER, true, true);
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				for(ModBase mod : Wrapper.getEnergy().mods.getActiveMods()) {
					if(!(mod.getNoCheat())) {
						mod.setState(false);
					}
				}
			}
		}
	}
}