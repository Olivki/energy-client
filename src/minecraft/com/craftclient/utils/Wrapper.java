package com.craftclient.utils;

import com.craftclient.Energy;

public class Wrapper {
	
	public static Client getClient() {
		return Client.getInstance();
	}
	
	public static Player getPlayer() {
		return Player.getInstance();
	}
	
	public static Energy getEnergy() {
		return Energy.getInstance();
	}
}