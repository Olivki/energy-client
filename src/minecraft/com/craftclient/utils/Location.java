package com.craftclient.utils;

public class Location {

	protected double x;
	
	protected double y;
	
	protected double z;
	
	public Location(double x, double y, double z) {
		setX(x);
		setY(y);
		setZ(z);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}
	
	@Override
	public String toString() {
		return "(" + (int)getX() + "," + (int)getY() + "," + (int)getZ() + ")";
	}
}