package com.craftclient.panels.list.panels;

import com.craftclient.panels.*;
import com.craftclient.panels.list.buttons.*;
import com.craftclient.utils.Wrapper;

public class PanelGuiHub extends PanelBase {

	public PanelGuiHub(int x, int y) {
		super("Gui Hub", x, y);
	}
	
	@Override
	public void initButtons() {
		for(PanelBase panel : Wrapper.getEnergy().getPanels().getLoadedQueuePanels()) {
			addButton(new ButtonPanel(panel.getName(), getWidth() - 6, 12, this, panel));
		}
	}
}