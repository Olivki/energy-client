package com.craftclient.panels.list.buttons;

import com.craftclient.mods.*;
import com.craftclient.panels.*;
import com.craftclient.utils.Client;
import com.craftclient.utils.Colours;
import com.craftclient.utils.Wrapper;

public class ButtonPanel extends ButtonBase {

	private PanelBase extraPanel;

	public ButtonPanel(String name, int width, int height, PanelBase panel, PanelBase extraPanel) {
		super(name, 0, 0, width, height, panel);
		setExtraPanel(extraPanel);
	}

	@Override
	public void draw(int x, int y) {
		drawGradientBorderedRect(getX(), getY(), getX() + getWidth() + 2, getY() + getHeight(), 0x80000000, 0xFF424242, 0xFF2E2E2E);
		Client.getIngameGui().drawCenteredStringS(Client.getFontRenderer(), getName(), getX() + getWidth() / 2, getY() + 2.5F, (isHovering(x, y) 
				? 0xFFFFFFA0 : (Wrapper.getEnergy().getPanels().getLoadedPanels().contains(getExtraPanel()) ? 0xFFFFFFFF : 0xFFAAAAAA)));
	}

	@Override
	public void mouseClicked(int x, int y, int type) {
		if(isHovering(x, y) && type == 0) {
			if(!(Wrapper.getEnergy().getPanels().getLoadedPanels().contains(getExtraPanel()))) {
				Wrapper.getEnergy().getPanels().addPanel(getExtraPanel());
			} else if(Wrapper.getEnergy().getPanels().getLoadedPanels().contains(getExtraPanel())) {
				Wrapper.getEnergy().getPanels().removePanel(getExtraPanel());
			}
			Client.playSound(1.0F);
		}
	}

	public PanelBase getExtraPanel() {
		return extraPanel;
	}

	public void setExtraPanel(PanelBase panel) {
		this.extraPanel = panel;
	}

	@Override
	public boolean isHovering(int x, int y) {
		return x >= getX() + 2 && y >= getY() && x <= getX() + getWidth() && y <= getY() + getHeight();
	}
}