package com.craftclient.panels.list.buttons;

import com.craftclient.mods.*;
import com.craftclient.panels.*;
import com.craftclient.utils.Client;
import com.craftclient.utils.Colours;
import com.craftclient.utils.Wrapper;

public class ButtonCenteredText extends ButtonBase {

	public ButtonCenteredText(String name, int width, int height, PanelBase panel) {
		super(name, 0, 0, width, height, panel);
	}

	@Override
	public void draw(int x, int y) {
		Client.getIngameGui().drawCenteredStringS(Client.getFontRenderer(), getName(), getX() + getWidth() / 2, getY() + 2.5F, 0xFFFFFFFF);
	}

	@Override
	public void drawTag(int x, int y) {
		
	}
	
	@Override
	public void mouseClicked(int x, int y, int type) {
		
	}
}