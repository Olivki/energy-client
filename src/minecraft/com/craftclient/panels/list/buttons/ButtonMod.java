package com.craftclient.panels.list.buttons;

import com.craftclient.mods.*;
import com.craftclient.panels.*;
import com.craftclient.utils.Client;
import com.craftclient.utils.Colours;
import com.craftclient.utils.Wrapper;

public class ButtonMod extends ButtonBase {

	private ModBase mod;

	public ButtonMod(String name, int width, int height, PanelBase panel, ModBase mod) {
		super(name, 0, 0, width, height, panel);
		setMod(mod);
	}

	@Override
	public void draw(int x, int y) {
		drawGradientBorderedRect(getX() + getWidth() - 8, getY(), getX() + getWidth() + 2, getY() + getHeight(), 
				0x80000000, 0xFF424242, 0xFF2E2E2E);
		Client.drawStringWithShadow(getName(), getX() + 1, getY() + 2.5F, (getMod().getState() ? 0xFFFFFFFF : 0xFFAAAAAA));
		Client.getIngameGui().drawCenteredStringS(Client.getFontRenderer(), (getMod().getState() ? "+" : "-"), getX() + getWidth() 
				- 1.5F - Client.getStringWidth(getMod().getState() ? "+" : "-") / 4, getY() + 2.5F, (isHovering(x, y) ? 0xFFFFFFA0 : 
					(getMod().getState() ? 0xFFFFFFFF : 0xFFAAAAAA)));
	}

	@Override
	public void mouseClicked(int x, int y, int type) {
		if(isHovering(x, y) && type == 0) {
			getMod().toggle();
			Client.playSound(1.0F);
		}
	}

	public ModBase getMod() {
		return mod;
	}

	public void setMod(ModBase mod) {
		this.mod = mod;
	}

	@Override
	public boolean isHovering(int x, int y) {
		return x >= getX() + getWidth() - 8 && y >= getY() && x <= getX() + getWidth() + 2 && y <= getY() + getHeight();
	}
}