package com.craftclient.events.controller;

import net.minecraft.src.Entity;

import com.craftclient.events.*;

public class EventAttackEntity extends EventCancellable {
	
	private Entity target;
	
	public EventAttackEntity(Entity target) {
		setTarget(target);
	}

	public Entity getTarget() {
		return target;
	}

	public void setTarget(Entity target) {
		this.target = target;
	}
}