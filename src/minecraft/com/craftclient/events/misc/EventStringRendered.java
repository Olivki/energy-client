package com.craftclient.events.misc;

import com.craftclient.events.*;

public class EventStringRendered extends Event {
	
	protected String string;
	
	protected int width;
	
	public EventStringRendered(String string) {
		setString(string);
	}
	
	public EventStringRendered(int width) {
		setWidth(width);
	}

	public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
}