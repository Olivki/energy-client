package com.craftclient.events.server;

import net.minecraft.src.*;
import com.craftclient.events.*;

public class EventChatReceive extends Event {
	
	protected String message;

	public EventChatReceive(String message) {
		this.setMessage(message);
	}
	
	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}