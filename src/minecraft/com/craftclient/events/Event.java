package com.craftclient.events;

public class Event {
	
	private EventPriority priority;
	
	public Event(EventPriority priority) {
		setPriority(priority);
	}
	
	public Event() {
		this(EventPriority.MEDIUM);
	}

	public EventPriority getPriority() {
		return priority;
	}

	public void setPriority(EventPriority priority) {
		this.priority = priority;
	}
}