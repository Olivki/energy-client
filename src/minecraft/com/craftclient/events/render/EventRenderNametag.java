package com.craftclient.events.render;

import net.minecraft.src.*;
import com.craftclient.events.*;

public class EventRenderNametag extends Event {
	
	protected double x;
	
	protected double y;
	
	protected double z;
	
	protected EntityPlayer player;
	
	protected RenderPlayer render;
	
	public EventRenderNametag(double x, double y, double z, EntityPlayer player, RenderPlayer render) {
		setX(x);
		setY(y);
		setZ(z);
		setPlayer(player);
		setRender(render);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public EntityPlayer getPlayer() {
		return player;
	}

	public void setPlayer(EntityPlayer player) {
		this.player = player;
	}

	public RenderPlayer getRender() {
		return render;
	}

	public void setRender(RenderPlayer render) {
		this.render = render;
	}
}