package com.craftclient.events.world;

import com.craftclient.events.*;

public class EventBlockDamaged extends Event {

	protected float speed;

	protected int delay;

	public EventBlockDamaged() {
		if(speed == 0 && delay == 0) {
			setSpeed(1.0F);
			setDelay(5);
		}
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		if(speed > 1.0F) {
			this.speed = 1.0F;
		} else {
			this.speed = speed;
		}
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		if(delay > 5) {
			this.delay = 5;
		} else {
			this.delay = delay;
		}
	}
}